package practica4Curs5;

public class act5App {

	public static void main(String[] args) {
		
		//Aqu� crear� variables
		
		int a = 1;
		int b = 2;
		int c = 3;
		int d = 4;
		
		//Aqu� ensenyar� el valor inicial de les variables
		System.out.println("Valor inicial a = " + a);
		System.out.println("Valor inicial b = " + b);
		System.out.println("Valor inicial c = " + c);
		System.out.println("Valor inicial d = " + d);
		System.out.println("-----------------------");
		//Aqu� mostrar� com les variables intercanvien els valors entre s�
		System.out.println("b con el valor c = " + (b=c));
		System.out.println("c con el valor a = " + (c=a));
		System.out.println("a con el valor d = " + (a=d));
		System.out.println("d con el valor b = " + (d=b));
		System.out.println("-----------------------");
		//Aqu� se puede ver como quedan las variables finalmente
		System.out.println("Valor final a = " + a);
		System.out.println("Valor final b = " + b);
		System.out.println("Valor final c = " + c);
		System.out.println("Valor final d = " + d);

	}

}
